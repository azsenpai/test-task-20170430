SELECT T1.event_type, (T1.value - T2.value) AS value
FROM (
	SELECT T2.*
	FROM (
		SELECT e.event_type, MAX(e.time) AS time
		FROM events e
		GROUP BY e.event_type
		HAVING COUNT(e.event_type) > 1
	) T1
	LEFT JOIN events T2
	ON T1.event_type = T2.event_type AND T1.time = T2.time
) T1
LEFT JOIN (
	SELECT T2.*
	FROM (
		SELECT T2.event_type, MAX(T2.time) AS time
		FROM (
			SELECT e.event_type, MAX(e.time) AS time
			FROM events e
			GROUP BY e.event_type
			HAVING COUNT(e.event_type) > 1
		) T1
		LEFT JOIN events T2
		ON T1.event_type = T2.event_type AND T1.time != T2.time
		GROUP BY T2.event_type
	) T1
	LEFT JOIN events T2
	ON T1.event_type = T2.event_type AND T1.time = T2.time
) T2
ON T1.event_type = T2.event_type
ORDER BY T1.event_type
